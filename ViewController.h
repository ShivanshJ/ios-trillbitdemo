//
//  ViewController.h
//  AudioQueueServicesExample
//
//  Created by Shivansh on 6/12/17.
//
//

#import <UIKit/UIKit.h>
#import "AudioRecorderAppDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreAudioKit/CoreAudioKit.h>

#import <AudioToolbox/Audiotoolbox.h>
#import <AudioToolbox/AudioQueue.h>
#import <AudioToolbox/AudioFile.h>

#include <FLAnimatedImage/FLAnimatedImage.h>
#include <FLAnimatedImage/FLAnimatedImageView.h>



#include "coupon.h"

//#### AUDIO FORMAT SETTINGS IN .pch file ###
// Struct defining recording state
typedef struct
{
    AudioStreamBasicDescription  dataFormat;
    AudioQueueRef                queue;
    AudioQueueBufferRef          buffers[NUM_BUFFERS];
    AudioFileID                  audioFile;
    UInt32                         mNumPacketsToRead;
    SInt64                       currentPacket;
    bool                         recording;
} RecordState;

// Struct defining playback state
typedef struct
{
    AudioStreamBasicDescription  dataFormat;
    AudioQueueRef                queue; //The playback audio queue created by your application.
    AudioQueueBufferRef          buffers[NUM_BUFFERS];  //An array holding pointers to the audio queue buffers managed by the audio queue.
    AudioFileID                  audioFile;     //An audio file object that represents the audio file your program plays.
    UInt32                        mNumPacketsToRead; //STOLEN :The number of packets to read on each invocation of the audio queue’s playback callback
    SInt64                       currentPacket; //The packet index for the next packet to play from the audio file.
    bool                         playing;
} PlayState;


//------------------------------------------------------------------------------
#pragma mark - Class
//------------------------------------------------------------------------------

@interface ViewController : UIViewController <AVAudioSessionDelegate>
{   //Globals
    NSString *_shorts;
    NSString *_decoded;
    
    //Others
    UILabel* labelStatus;
    UIButton* buttonRecord;
    UIButton* buttonPlay;
    RecordState recordState;
    PlayState playState;
    CFURLRef fileURL;
}

//Our functions
@property (nonatomic, strong) AudioRecorderAppDelegate *appDelegate; //Global array declaration
@property(strong, nonatomic, readwrite) NSString *shorts;
@property(strong, nonatomic, readwrite) NSString *decoded;
+ (ViewController *) sharedInstance;


//Audio Functions
- (BOOL)getFilename:(char*)buffer maxLenth:(int)maxBufferLength;
- (void)setupAudioFormat:(AudioStreamBasicDescription*)format;
- (void)recordPressed:(id)sender;
- (void)playPressed:(id)sender;
- (void)startRecording;
- (void)stopRecording;
- (void)startPlayback;
- (void)stopPlayback;

- (void)feedSamplesToEngine:(UInt32)audioDataBytesCapacity audioData:(void *)audioData;
/////////////////////


@property (retain, nonatomic) IBOutlet UILabel *TextView;
@property (strong, nonatomic) IBOutlet FLAnimatedImageView *gifcircle;
@property (strong, nonatomic) IBOutlet FLAnimatedImageView *gifwave;

//########################Pods/Carthage

@property (nonatomic, strong) NSArray *myArray;
@property (nonatomic) NSInteger iteration;

@end
