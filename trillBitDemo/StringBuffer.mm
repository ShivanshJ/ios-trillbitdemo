//
//  StringBuffer.cpp
//  AudioQueue
//
//  Created by Shivansh on 6/23/17.
//
//
#import "ViewController.h"

#include "StringBuffer.hpp"
#include "Finddata.hpp"


int StringBuffer::putshortsinarray(string const& shorts, int &buffer_count, double result)
{
    cout<<"Flag: "<<flag;
    
    c_buffer[buffer_count].clear();    //If our buffer is filled, clear it
    c_buffer[buffer_count] = shorts;   //write to our buffer
    
    //Enters everytime after trigger is detected
    if(flag==1)
    {
        return WritetoStorage(buffer_count);

    }
    
    else if(result>75)
    {   flag=1;
        
        //When trigger is detected, start writing to @storage[]
        //To reduce comparison computation, not sending to Writetostorage()
        
        //Records previous 2 buffers, after the trigger is detected
        for(int j=mBUFFERS_BEFORE_TRIGGER; j>=1;j--)
        {
            int temp_buffer_count = (buffer_count-j < 0)? (STORAGE_VECTOR + buffer_count - j) : (buffer_count - j);
            storage[storagect++] = c_buffer[temp_buffer_count] ;
        }
        storage[storagect++] = c_buffer[buffer_count] ;

        return 0;   //So that recording continues
    }
    else
        return 0;

    
    
}


int StringBuffer::WritetoStorage(int buffer_count) //Its &storagect otherwise local storagect is made
{
    cout<<storagect<<endl;
    
    if(storagect == TOTAL_CHUNKS_HAVING_DATA)
    {   flag=0;
        storagect = 0;
        string allBufferString ;
        for(int j=0; j<TOTAL_CHUNKS_HAVING_DATA ;j++)
        {   //cout<<"\n################Storage chunk :"<<j<<" "<<storage[j];
            allBufferString.append(storage[j]);
        }
        
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
//        ViewController *vc = [storyboard instantiateInitialViewController];
//        
//        NSString *BufferString = [[NSString alloc] initWithCString:allBufferString.c_str()
//                                                    encoding:[NSString defaultCStringEncoding]];
//        
//        cout<<allBufferString;
//        cout<<endl<<allBufferString.length();
//        [vc storeToShorts:BufferString];
        
        return divideDataby();
            
    }
    else
        {
            storage[storagect++]=c_buffer[buffer_count];
            return 0;
        }
    
}



//Dividing total number of chunks in storage[], da
int StringBuffer::divideDataby()
{
    int number = TOTAL_CHUNKS_HAVING_DATA/DIVIDE_TOTAL_CHUNKS_INTO;
    
    string divided_storage[DIVIDE_TOTAL_CHUNKS_INTO];
    int counter = 0; //Count for seeing chunks
    
    int temp_DIVIDE = DIVIDE_TOTAL_CHUNKS_INTO;
    
    int ct_flag;
    while(temp_DIVIDE--)
    {   ct_flag=0;
        F(i, counter, TOTAL_CHUNKS_HAVING_DATA )//cant do cuz i will always be 0
        {   if(i%number==0)
                ct_flag++;
            
            if(ct_flag==2)
                    break;
            
            divided_storage[temp_DIVIDE].append(storage[i]) ;
            counter++;
            }
        
    }
    
    //#FUTURE WORK - Send data asynchronously
    
    FindData fd;// Class name for findata funcitons
    std::pair<int,int> pair_result[DIVIDE_TOTAL_CHUNKS_INTO];
    F(i,0,DIVIDE_TOTAL_CHUNKS_INTO)
        {
            pair_result[i] = fd.apply( divided_storage[i] );
            cout<<"\n\nFINAL RESULT::"<<pair_result[i].first;
            
            if( (i!=0 && (pair_result[i].first==pair_result[i-1].first) )|| ( (i-2)>=0 && (pair_result[i].first==pair_result[i-1].first)) )
            {   //Send : pair_result[i].first;
                cout<<"\nMAjority RESULT::"<<pair_result[i].first<<endl;
                return pair_result[i].first;
                break;
            }
            
        }
    return pair_result[2].first;
    

}
