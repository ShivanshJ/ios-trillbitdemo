//
//  ViewController.swift
//  Example-iOS
//
//  Created by Jonathan on 9/4/16.
//  Copyright © 2016 Jonathan Underwood. All rights reserved.
//

import UIKit
import AVFoundation
import WaveformView

var flag : NSInteger = 0
var ct : Double = 0.0

@objc class WaveViewController: UIViewController {
    var audioRecorder: AVAudioRecorder!
    
    
    @IBOutlet weak var waveformView: WaveformView!
    
    //override func viewAppear(_ animated: Bool) {
    override func viewDidLoad(){
        //super.viewWillAppear(animated)
        super.viewDidLoad()
        waveformView.waveColor = hexStringToUIColor(hex: "#3498DB")
        
        //audioRecorder = audioRecorder(URL(fileURLWithPath:"/dev/null"))
        //audioRecorder.record()

        let displayLink = CADisplayLink(target: self, selector: #selector(updateMeters))
        displayLink.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
    }
    

    func updateMeters() {
        //audioRecorder.updateMeters()
        //NSLog("\n updateMeters()")
        let normalizedValue : Double
        if flag == 1 {
            if ct < 1.2 {
                ct = ct + 0.08
            }
        let raise = CGFloat(-1.4 + ct) //- CGFloat(audioRecorder.averagePower(forChannel: 0)/30)
        normalizedValue = Double( pow(10, raise) )
        }
        else {
            if ct > 0 {
                ct = ct - 0.015
            }
        normalizedValue = pow(10, -1.4 + ct)
        }
        
        waveformView.updateWithLevel(CGFloat(normalizedValue))
    }

    func audioRecorder(_ filePath: URL) -> AVAudioRecorder {
        let recorderSettings: [String : AnyObject] = [
            AVSampleRateKey: 44100.0 as AnyObject,
            AVFormatIDKey: NSNumber(value: kAudioFormatMPEG4AAC),
            AVNumberOfChannelsKey: 1 as AnyObject,
            AVEncoderAudioQualityKey: AVAudioQuality.min.rawValue as AnyObject
        ]

        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)

        let audioRecorder = try! AVAudioRecorder(url: filePath, settings: recorderSettings)
        audioRecorder.isMeteringEnabled = true
        audioRecorder.prepareToRecord()

        return audioRecorder
    }
    //Comes from appDelegate
    func ChangeSwiftFlag( tflag: NSInteger) {        
        flag = tflag;

    }
    
    
//Testers
    func hell() {
    }
//Stackoverflow for hex to uicolour
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}
