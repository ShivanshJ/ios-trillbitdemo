//
//  coupon.h
//  trillBitDemo
//
//  Created by Shivansh on 9/28/17.
//

#import <UIKit/UIKit.h>

@interface coupon : UIViewController
- (IBAction)back_button:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *background_img;
@property (nonatomic, strong) UIImage *destination_img;
@property (nonatomic, strong) NSString *identify_segue;



@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *img_col;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *text_col;

@end
