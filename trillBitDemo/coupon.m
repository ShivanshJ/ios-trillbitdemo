//
//  coupon.m
//  trillBitDemo
//
//  Created by Shivansh on 9/28/17.
//

#import "coupon.h"

@interface coupon ()

@end

@implementation coupon

@synthesize background_img;
@synthesize identify_segue;
@synthesize text_col;
@synthesize img_col;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.background_img.image = self.destination_img;
    
    
    
    if([self.identify_segue isEqualToString:@"coupon"]){
        NSLog(@"perfect viewdidload");
    }
    else {
        for (UILabel *t in self.text_col)
            t.hidden = YES;
        for (UIImageView *t in self.img_col)
            [t setHidden:YES];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back_button:(id)sender {
    
   [self.navigationController popViewControllerAnimated:YES];
}
@end
