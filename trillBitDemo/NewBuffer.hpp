//
//  NewBuffer.hpp
//  AudioServerQueue
//
//  Created by Shivansh on 7/8/17.
//
//
#ifdef __cplusplus
#include <string>
#include <iostream>
using namespace std;
#endif

#ifndef NewBuffer_hpp
#define NewBuffer_hpp

#include <stdio.h>

class NewBuffer {
    std::string storage[TOTAL_CHUNKS_HAVING_DATA];            //@storage is the final array of chunks of our whole data
    int flag;
    int storagect;
    string c_buffer[STORAGE_VECTOR];
    
    //@allBufferString is used here to save storage array in one vector
    //@allpairDecoded stores decoded stuff as string
    string allBufferString ;
    string allpairDecoded ;
    public:
    
    
    NewBuffer()  //Constructor
    {flag=0; storagect=0;}
    
    int putshortsinarray(string const& shorts, int &buffer_count, double result);
    int WritetoStorage(int buffer_count);
    int divideDataby();
    
    void sendToView();
    
    //@ putshortsinarray() - Puts data into temporary array buffer,c_buffer
    //@ WritetoStorage() - After trigger is detected, the subsequent TOTAL_CHUNKS_HAVING_DATA are recorded into storage array
    //@ divideDataby() - Divides total data to be taken in a few parts, and retirieves data
};

#endif /* NewBuffer_hpp */
