The third commit:

------Changes in FindData
Includes optimized version of three sorting algorithms,
0. Function takes corrOut,size of correlated vec, and array where storage is done
1. Has sort5, sort7 and sort10 versions.
2. DP cache has been commented out
3. Makes changes to an empty array being passed

Class linked_list(doubly) introduced:
1. Acts as a hash map to store each unique number decoded as a 'node'
2. Contains 'count' of the number.
3. Has a function to return majority element

-------Changes in ShortBuffer.mm
1. Introduced a 1d array of size 3 to store results decoded.
2. Pasted the result in an array of size 9, which goes to an outer scope.

-------
