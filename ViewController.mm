//
//  ViewController.m
//  AudioQueueServicesExample
//
//  Created by Shivansh on 6/12/17.
//
//


#import "ViewController.h"

#import "Trigerring.hpp"                //Can only declare in .mm file,else error
#include "ShortBuffer.hpp"         //Can only declare in .mm file



#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#define kDestinationURL @"http://192.168.1.23:8081/v1/iphone/audio/info/"

@interface ViewController ()
{ AVAudioSession *session;
}
@end

dispatch_queue_t concurrentQueue;
//My concurrent thread, for asynchronous task, declared outside so that can be accessed inside callback functions

// Declare C callback functions
void AudioInputCallback(void * inUserData,  // Custom audio metadata
                        AudioQueueRef inAQ,
                        AudioQueueBufferRef inBuffer,
                        const AudioTimeStamp * inStartTime,
                        UInt32 inNumberPacketDescriptions,
                        const AudioStreamPacketDescription * inPacketDescs);

void AudioOutputCallback(void * inUserData,
                         AudioQueueRef outAQ,
                         AudioQueueBufferRef outBuffer);


#pragma mark

@implementation ViewController
@synthesize TextView;


//Global declaration bitches
@synthesize appDelegate;
@synthesize shorts = _shorts;
@synthesize decoded = _decoded;
//------------------------------------------------------------------------------
#pragma mark - LOADER
//------------------------------------------------------------------------------
+ (ViewController *)sharedInstance {
    static dispatch_once_t onceToken;
    static ViewController *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[ViewController alloc] init];
    });
    return instance;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    appDelegate = (AudioRecorderAppDelegate *)[[UIApplication sharedApplication] delegate];
    //This ensures it plays properly on speaker output
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord
                                     withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker
                                           error:nil];
    refToSelf = (__bridge void*)self; //NOTE : MADE FOR SAMPLES TO ENGINE function in AudioInputCallback
    //..Get audio file path
    char path[256];
    [self getFilename:path maxLenth:sizeof path];
    fileURL = CFURLCreateFromFileSystemRepresentation(NULL, (UInt8*)path, strlen(path), false);
    // Init state variables
    playState.playing = false;
    recordState.recording = false;
    
    //Text Properties
    //self.TextView.backgroundColor = [UIColor whiteColor];   //Without setting this, text was overwritten.
    //self.TextView.text=@"";
    
    //Global SharedInstance
    _shorts = nil;
    _decoded = nil;
    //##MYTHREAD
    //concurrentQueue = dispatch_queue_create("MyQ1", DISPATCH_QUEUE_CONCURRENT);
    concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    
    //GIF-----------------
    FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"siri" ofType:@"gif"]]];
    self.gifcircle.animatedImage = image;
    [self.view addSubview:self.gifcircle];
    //-------------------
    
    NSBundle *bundle = [NSBundle mainBundle];
    NSLog(@"\nResource path for test.applescript: %@", [bundle pathForResource:@"siri" ofType:@"gif"]);
    
    self.myArray = [NSArray arrayWithObjects:@"Looking for Trill Tones",
                    @"Finding Contextual contents",
                    @"Unlocking new experiences",
                    @"Sensing Proximity",
                    @"Powered by SOUND",nil];
    self.iteration = -1;
    
    [NSTimer scheduledTimerWithTimeInterval:4
                                     target:self
                                   selector:@selector(fireEvent)
                                   userInfo:nil
                                    repeats:YES]; //Will Fire every 3 seconds...
}


-(void) viewDidAppear:(BOOL)animated{
    [appDelegate ChangeFlagSwift:0]; //Changing flag for the wave
    //gIF IMAGE
    [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.2]];
    [self startrec:nil];
}


-(void)fireEvent
{   self.iteration++;
    
    NSLog(@"Iteration %@ %d", [self.myArray objectAtIndex:self.iteration], self.iteration);
    //FadeOut
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:0.3];
    //[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    self.TextView.alpha = 0;
    [UIView commitAnimations];
    self.TextView.text = [self.myArray objectAtIndex:self.iteration];
    //FADEin
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:0.8];
    //[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    self.TextView.alpha = 1;
    [UIView commitAnimations];
    
    
    if (self.iteration>=self.myArray.count-1) self.iteration=-1;
}


//------------------------------------------------------------------------------
#pragma mark - Buttons
//------------------------------------------------------------------------------
- (IBAction)startrec:(UIButton *)sender {
    check_result_flag = 0;
    [self startRecording];
}

- (IBAction)stoprec:(UIButton *)sender {
    [self stopRecording];
}

- (IBAction)playrec:(UIButton *)sender {
    [self startPlayback];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//##########################################################################################
#pragma mark
//------------------------------------------------------------------------------
#pragma mark - AUDIO FUNCTIONS
//---------------------------------
#pragma mark - feedSamples
//------------------------------------------------------------------------------
ShortBuffer strbuffer;            //OBJECT for Stringbuffer class
void *refToSelf;
int ct=0;
int check_result_flag=0;
//@ct -> Our own count for circular buffer for storing "shorts" in string, value goes upto @STORAGE_VECTOR
//@check_result_flag -> Gives back OTP result as well as other flags
//                   0 -> Continue to check trigger values for every @BUFFER_SIZE "shorts"
//                  -3 -> When trigger value is detected
//                  -4 -> When the next @TOTAL_CHUNKS_HAVING_DATA are being stored after detecting -3
//                  -5 -> When the final buffer is stored, so that ASYNCHRONOUS thread can be run when CORRELATION calculations will happen
//                  -2 -> When calculation for OTP is going on
//                  >0 -> OTP detected ; -1 -> Wrong OTP

- (void)feedSamplesToEngine:(UInt32)audioDataBytesCapacity audioData:(void *)audioData
{
    if(audioDataBytesCapacity > BUFFER_SIZE)
        return; //#Handles error where overflow is there.
    
    int sampleCount = audioDataBytesCapacity / sizeof(SAMPLE_TYPE);
    SAMPLE_TYPE *samples = (SAMPLE_TYPE*)audioData;
    std::string shorts;
    double power = pow(2,10);
    for(int i = 0; i < sampleCount; i++) {
        //#EXTREME IMPORTANCE: the data recieved is in a format where the endianess,i.e, format of binary is in a different way
        //This is done to correct that error for "16BIT" binary number
        SAMPLE_TYPE sample_swap =  (0xff00 & (samples[i] << 8)) | (0x00ff & (samples[i] >> 8)) ; //Endianess issue
        char dataInterim[30];
        sprintf(dataInterim,"%f ", sample_swap/power); // normalize it.
        shorts.append(dataInterim);
    }
    //Checking trigger
    double trigger_result =1;
    if(check_result_flag==0){
        Trigger tg; //initiated for later use
        trigger_result = tg.isFound(shorts);
        NSLog(@"\nTrigger for chunk no. %d : %f",++ct, trigger_result);
    }
    //Sending to paste to our own circular buffer
    ct = ct%(STORAGE_VECTOR);
    
    //*********@check :
    //*********0:continue recording, -2:Final Calculation of OTP going on, -3:Point where trigger is detected,-4 means dont read trigger next time as value of check is global
    int send_to_corr;
    send_to_corr = strbuffer.putshortsinarray(shorts, ct, trigger_result);
    check_result_flag = send_to_corr;
    dispatch_async(dispatch_get_main_queue(), ^{
        if(check_result_flag == -2 ){
            [self stopRecording];
            NSLog(@"Stopping recording, entry in -2");
        }
        if(check_result_flag>0){
            [self stopRecording]; //NOTE: Enabling this here and inside check==-2 , throws ERROR
            [appDelegate ChangeFlagSwift:0];
            if(check_result_flag == 3730)
                [self performSegueWithIdentifier:@"coupon" sender:self];
            else if(check_result_flag == 3960)
                [self performSegueWithIdentifier:@"nike_offer" sender:self];
            else if(check_result_flag == 4190)
                [self performSegueWithIdentifier:@"asiaNet" sender:self];
            else if(check_result_flag == 4420)
                [self performSegueWithIdentifier:@"authenticate" sender:self];
            else if(check_result_flag == 4880)
                [self performSegueWithIdentifier:@"trillbit" sender:self];
            else if(check_result_flag == 5110)
                [self performSegueWithIdentifier:@"One" sender:self];
            else if(check_result_flag == 6490)
                [self performSegueWithIdentifier:@"Two" sender:self];
            else if(check_result_flag == 6720)
                [self performSegueWithIdentifier:@"Three" sender:self];
            else if(check_result_flag == 4650)
                [self performSegueWithIdentifier:@"REL1" sender:self];
            else if(check_result_flag == 5570)
                [self performSegueWithIdentifier:@"REL2" sender:self];
            else {
                [self startRecording];
                [appDelegate ChangeFlagSwift:0];
            }
        }
        else if(check_result_flag==-3){
            [appDelegate ChangeFlagSwift:1];
        }
        else if (check_result_flag==-1){
            [self startRecording];
           
            [appDelegate ChangeFlagSwift:0];
        }
    });

}



//------------------------------------------------------------------------------
#pragma mark - CallBack
//------------------------------------------------------------------------------
//.........................................................................................................
//##########################################################################################

// Takes a filled buffer and writes it to disk, "emptying" the buffer
void AudioInputCallback(void * inUserData,
                        AudioQueueRef inAQ,
                        AudioQueueBufferRef inBuffer,
                        const AudioTimeStamp * inStartTime,
                        UInt32 inNumberPacketDescriptions,
                        const AudioStreamPacketDescription * inPacketDescs)
{
    RecordState * recordState = (RecordState*)inUserData;
    if (!recordState->recording)
    {
        printf("Not recording, returning\n");
        return; //important otherwise takes global value of check in its other buffers.
    }
    //     if (inNumberPacketDescriptions == 0 && recordState->dataFormat.mBytesPerPacket != 0)
    //     {
    //         inNumberPacketDescriptions = inBuffer->mAudioDataByteSize / recordState->dataFormat.mBytesPerPacket;
    //     }
    printf("Writing buffer %lld\n", recordState->currentPacket);
    OSStatus status = AudioFileWritePackets(recordState->audioFile,
                                            false,
                                            inBuffer->mAudioDataByteSize,
                                            inPacketDescs,
                                            recordState->currentPacket,
                                            &inNumberPacketDescriptions,
                                            inBuffer->mAudioData);
    if (status == 0) {
        recordState->currentPacket += inNumberPacketDescriptions;
    }
    else cout<<"Status:"<<status;
    AudioQueueEnqueueBuffer(recordState->queue, inBuffer, 0, NULL);
    //Do stuff with samples recorded
    if(check_result_flag == -4 ){
        NSDate *date = [NSDate date];
        ViewController *rec = (__bridge ViewController *) refToSelf;
        [rec feedSamplesToEngine:inBuffer->mAudioDataBytesCapacity audioData:inBuffer->mAudioData];
        double timePassed_ms = [date timeIntervalSinceNow] * -1000.0;
        if(timePassed_ms>31)
            cout<<"\nTIME: "<<timePassed_ms;
    }
    else {
        dispatch_async(concurrentQueue, ^{
            cout<<"in";
            NSDate *date = [NSDate date];
            ViewController *rec = (__bridge ViewController *) refToSelf;
            [rec feedSamplesToEngine:inBuffer->mAudioDataBytesCapacity audioData:inBuffer->mAudioData];
            double timePassed_ms = [date timeIntervalSinceNow] * -1000.0;
            if(timePassed_ms>31)
                cout<<"\nTIME: "<<timePassed_ms;
        });
    }
}

//....................................... Fills an empty buffer with data and sends it to the speaker
void AudioOutputCallback(void * inUserData,
                         AudioQueueRef outAQ,
                         AudioQueueBufferRef outBuffer)
{
    PlayState* playState = (PlayState*)inUserData;
    if(!playState->playing)
    {
        printf("Not playing, returning\n");
        return;
    }
    
    printf("Queuing buffer %lld for playback\n", playState->currentPacket);
    
    
    AudioStreamPacketDescription* packetDescs = NULL;
    
    
    UInt32 bytesRead;
    UInt32 numPackets = BUFFER_SIZE/2;
    //playState->mNumPacketsToRead ; //BUFFER_SIZE/2; because see sampleCount is 2048 in feedSamplesToEngine function
    OSStatus status;
    status = AudioFileReadPackets(playState->audioFile,
                                  false,
                                  &bytesRead,
                                  packetDescs,
                                  playState->currentPacket,
                                  &numPackets,
                                  outBuffer->mAudioData);
    //cout<<numPackets;
    if (numPackets>0)
    {
        outBuffer->mAudioDataByteSize = bytesRead;
        //status = AudioQueueEnqueueBuffer(outAQ, outBuffer, 0, NULL);
        status = AudioQueueEnqueueBuffer(playState->queue,
                                         outBuffer,
                                         0,
                                         packetDescs);
        
        playState->currentPacket += numPackets;
    }
    else
    {
        if (playState->playing)
        {
            AudioQueueStop(playState->queue, false);
            AudioFileClose(playState->audioFile);
            playState->playing = false;
        }
        
        AudioQueueFreeBuffer(playState->queue, outBuffer);
    }
    
}
//.##########################################################################################..

//------------------------------------------------------------------------------
#pragma mark - AudioFormat
//------------------------------------------------------------------------------

- (void)setupAudioFormat:(AudioStreamBasicDescription*)format
{
    //SAMPLE_TYPE is short datatype
    format->mSampleRate = SAMPLE_RATE;
    format->mFormatID = kAudioFormatLinearPCM;
    format->mFramesPerPacket = 1;
    format->mChannelsPerFrame = 1;
    format->mBytesPerFrame = sizeof(SAMPLE_TYPE) * NUM_CHANNELS; //2;
    format->mBytesPerPacket = sizeof(SAMPLE_TYPE) * NUM_CHANNELS;    //2;
    format->mBitsPerChannel = 8 * sizeof(SAMPLE_TYPE);          //16;
    format->mReserved = 0;
    format->mFormatFlags = kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked | kLinearPCMFormatFlagIsBigEndian ;
    
}

- (void)recordPressed:(id)sender
{
    if (!playState.playing)
    {
        if (!recordState.recording)
        {
            printf("Starting recording\n");
            [self startRecording];
        }
        else
        {
            printf("Stopping recording\n");
            [self stopRecording];
        }
    }
    else
    {
        printf("Can't start recording, currently playing\n");
    }
}

- (void)playPressed:(id)sender
{
    if (!recordState.recording)
    {
        if (!playState.playing)
        {
            printf("Starting playback\n");
            [self startPlayback];
        }
        else
        {
            printf("Stopping playback\n");
            [self stopPlayback];
        }
    }
}

//#############################

- (void)startRecording
{
    [self setupAudioFormat:&recordState.dataFormat];
    NSLog(@"Sample : %f",recordState.dataFormat.mSampleRate);
    
    recordState.currentPacket = 0;
    
    
    OSStatus status;
    status = AudioQueueNewInput(&recordState.dataFormat,
                                AudioInputCallback,
                                &recordState,
                                CFRunLoopGetCurrent(),
                                kCFRunLoopCommonModes,
                                0,
                                &recordState.queue);
    if (status == 0)
    {
        // Prime recording buffers with empty data
        for (int i = 0; i < NUM_BUFFERS; i++)
        {
            AudioQueueAllocateBuffer(recordState.queue, BUFFER_SIZE , &recordState.buffers[i]);
            AudioQueueEnqueueBuffer (recordState.queue, recordState.buffers[i], 0, NULL);
        }
        
        status = AudioFileCreateWithURL(fileURL, //if returns -50, then fileURL= " "should have path
                                        kAudioFileAIFFType,
                                        &recordState.dataFormat,
                                        kAudioFileFlags_EraseFile,
                                        &recordState.audioFile);
        
        if (status == 0)
        {   NSLog(@"Entered");
            recordState.recording = true;
            status = AudioQueueStart(recordState.queue, NULL);
            if (status == 0)
            {
                labelStatus.text = @"Recording";
            }
        }
    }
    
    if (status != 0)
    {
        NSLog(@"Statys : %d",(int)status);
        [self stopRecording];
        labelStatus.text = @"Record Failed";
    }
    
    
}

- (void)stopRecording
{
    recordState.recording = false;
    
    AudioQueueStop(recordState.queue, true);
    for(int i = 0; i < NUM_BUFFERS; i++)
    {
        AudioQueueFreeBuffer(recordState.queue, recordState.buffers[i]);
    }
    
    AudioQueueDispose(recordState.queue, true);
    AudioFileClose(recordState.audioFile);
    labelStatus.text = @"Idle";
}

//#####################################

- (void)startPlayback
{
    playState.currentPacket = 0;
    [self setupAudioFormat:&playState.dataFormat];
    OSStatus status;
    status = AudioFileOpenURL(fileURL, kAudioFileReadPermission, kAudioFileAIFFType, &playState.audioFile);
    
    if (status == 0) {
        status = AudioQueueNewOutput(&playState.dataFormat,
                                     AudioOutputCallback,
                                     &playState,
                                     CFRunLoopGetCurrent(),
                                     kCFRunLoopCommonModes,
                                     0,
                                     &playState.queue);
        if (status == 0) {
            // Allocate and prime playback buffers
            playState.playing = true;
            for (int i = 0; i < NUM_BUFFERS && playState.playing; i++)
            {
                
                AudioQueueAllocateBuffer(playState.queue, BUFFER_SIZE, &playState.buffers[i]);
                AudioOutputCallback(&playState, playState.queue, playState.buffers[i]);
            }
            
            status = AudioQueueStart(playState.queue, NULL);
            if (status == 0)
            {
                labelStatus.text = @"Playing";
            }
        }
    }
    
    if (status != 0)
    {
        [self stopPlayback];
        labelStatus.text = @"Play failed";
    }
}

- (void)stopPlayback
{
    playState.playing = false;
    
    for(int i = 0; i < NUM_BUFFERS; i++)
    {
        AudioQueueFreeBuffer(playState.queue, playState.buffers[i]);
    }
    
    AudioQueueDispose(playState.queue, true);
    AudioFileClose(playState.audioFile);
}

//------------------------------------------------------------------------------
#pragma mark - END
//------------------------------------------------------------------------------

- (BOOL)getFilename:(char*)buffer maxLenth:(int)maxBufferLength
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString* docDir = [paths objectAtIndex:0];
    NSString* file = [docDir stringByAppendingString:@"/recording.aif]"];
    return [file getCString:buffer maxLength:maxBufferLength encoding:NSUTF8StringEncoding];
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSLog(@"Enter - ");
    if( [segue.identifier isEqualToString:@"nike_offer"]){
        NSLog(@"nike call");
        coupon *container = [segue destinationViewController];
        container.destination_img = [UIImage imageNamed : @"nike_small"];
        container.identify_segue = @"nike_offer";
    }
    else if( [segue.identifier isEqualToString:@"coupon"]){
        NSLog(@"coupon call");
        coupon *container = [segue destinationViewController];
        container.destination_img = [UIImage imageNamed : @"Fans"];
        container.identify_segue = @"coupon";
    }
    else if( [segue.identifier isEqualToString:@"authenticate"]){
        NSLog(@"authenticate call");
        coupon *container = [segue destinationViewController];
        container.destination_img = [UIImage imageNamed : @"Bose_museum"];
        container.identify_segue = @"authenticate";
    }
    else if( [segue.identifier isEqualToString:@"trillbit"]){
        NSLog(@"trillbit");
        coupon *container = [segue destinationViewController];
        container.destination_img = [UIImage imageNamed : @"Bose_cafe"];
        container.identify_segue = @"trillbit";
    }
    else if( [segue.identifier isEqualToString:@"asiaNet"]){
        NSLog(@"asianet call");
        coupon *container = [segue destinationViewController];
        container.destination_img = [UIImage imageNamed : @"GST"];
        container.identify_segue = @"asiaNet";
    }
    else if( [segue.identifier isEqualToString:@"One"]){
        coupon *container = [segue destinationViewController];
        container.destination_img = [UIImage imageNamed : @"HDFC1"];
        container.identify_segue = @"One";
    }
    else if( [segue.identifier isEqualToString:@"Two"]){
        coupon *container = [segue destinationViewController];
        container.destination_img = [UIImage imageNamed : @"HDFC2"];
        container.identify_segue = @"Two";
    }
    else if( [segue.identifier isEqualToString:@"Three"]){
        coupon *container = [segue destinationViewController];
        container.destination_img = [UIImage imageNamed : @"HDFC3"];
        container.identify_segue = @"Three";
    }
    else if( [segue.identifier isEqualToString:@"REL1"]){
        coupon *container = [segue destinationViewController];
        container.destination_img = [UIImage imageNamed : @"reliance_trends_1"];
        container.identify_segue = @"REL1";
    }
    else if( [segue.identifier isEqualToString:@"REL2"]){
        coupon *container = [segue destinationViewController];
        container.destination_img = [UIImage imageNamed : @"reliance_trends_2"];
        container.identify_segue = @"REL2";
    }
    
}




@end

